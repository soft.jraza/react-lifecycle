import React, { Component } from "react"
import "./App.css"

class App extends Component {
  constructor(props) {
    super(props)
    this.state = { favouritecolor: "black" }
    console.log("constructor")
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    console.log("Mounting/Updating: getDerivedStateFromProps method is called")
    return null
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({ favouritecolor: "red" })
    }, 1000)

    console.log("Mounting phase: componentDidMount method is called")
  }

  shouldComponentUpdate() {
    console.log("Updating phase: shoulComponentUpdate method is called")
    return true
  }

  getSnapshotBeforeUpdate(prevProps, prevState) {
    document.getElementById("before").innerHTML =
      "before the update, Favourite color was " + prevState.favouritecolor
    console.log("Updating Phase: getSnapshotBeforeUpdate method is called")
    return null
  }

  componentDidUpdate() {
    document.getElementById("after").innerHTML =
      "after the update, Favourite color is " + this.state.favouritecolor
    console.log("Updating phase: componentDidUpdate method is called")
  }

  render() {
    console.log("render")
    return (
      <div>
        <h1>My Favourite Color is {this.state.favouritecolor} </h1>
        <div id='before'></div>
        <div id='after'></div>
      </div>
    )
  }
}

export default App
